#!/bin/bash
#
# Author: Luís de Sousa (luis.de.sousa[@]protonmail.ch)
# Date: 25-01-2019

echo "Installing lxappearance - theme management"
yes | apt install lxappearance

echo "Installing arandr - display settings manager"
yes | apt install arandr

echo "Installing feh - wallpaper setter"
yes | apt install feh

# Check for latest packages through this answer:
# https://askubuntu.com/a/1088268/177437
echo "Installing playerctl - media controls" 
cd tmp
wget http://ftp.nl.debian.org/debian/pool/main/p/playerctl/playerctl_2.0.1-1_amd64.deb
wget http://ftp.nl.debian.org/debian/pool/main/p/playerctl/libplayerctl2_2.0.1-1_amd64.deb
dpkg -i libplayerctl2_2.0.1-1_amd64.deb
dpkg -i playerctl_2.0.1-1_amd64.deb 

# Clean up
rm *.deb*

echo "All done!"
