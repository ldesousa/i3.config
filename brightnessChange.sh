#!/bin/bash

BRIGHTNESS=`cat brightness.txt`
if [[ -z "$BRIGHTNESS" ]]; then
	BRIGHTNESS=1
fi

BRIGHTNESS=`bc -l <<< $BRIGHTNESS$1`
xrandr --output eDP-1 --brightness $BRIGHTNESS

echo "$BRIGHTNESS" > brightness.txt
